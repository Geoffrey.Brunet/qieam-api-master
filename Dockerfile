#
# Build stage
#
FROM maven:3.6.3-openjdk-17-slim AS build
WORKDIR /home/qieam-api
COPY src /home/qieam-api/src
COPY pom.xml /home/qieam-api
RUN mvn -f /home/qieam-api/pom.xml clean package -DskipTests

#
# Package stage
#
FROM openjdk:17.0-slim
COPY --from=build /home/qieam-api/target/qieam-api-1.0-SNAPSHOT.jar /usr/local/lib/qieam-api-1.0-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/qieam-api-1.0-SNAPSHOT.jar"]
